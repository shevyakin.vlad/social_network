from rest_framework import (
    generics,
    status,
    permissions,
)

from rest_framework.authentication import (
    SessionAuthentication,
    BasicAuthentication,
)
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.views import ObtainJSONWebToken
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from django.contrib.auth import get_user_model

from django.db.models import Count
from django.utils.timezone import now
from django_filters import rest_framework as filters

from .models import (
    Post,
    Like,
    User,
)
from .serializers import (
    UserSerializer,
    PostSerializer,
    LikeSerializer,
    LikeAnalyticsSerializer,
    UserActivitySerializer,
)


class LikeFilter(filters.FilterSet):
    date_from = filters.DateFilter(field_name="liked_date", lookup_expr="gte")
    date_to = filters.DateFilter(field_name="liked_date", lookup_expr="lte")

    class Meta:
        model = Like
        fields = ["liked_date"]


class CreateUserAPIView(generics.CreateAPIView):
    model = get_user_model()
    permission_classes = [permissions.AllowAny]
    serializer_class = UserSerializer


class CreatePostAPIView(generics.CreateAPIView):
    model = Post
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    authentication_classes = (
        JSONWebTokenAuthentication,
        SessionAuthentication,
        BasicAuthentication,
    )
    serializer_class = PostSerializer


class CreateLikeAPIView(generics.CreateAPIView):
    model = Like
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    authentication_classes = (
        JSONWebTokenAuthentication,
        SessionAuthentication,
        BasicAuthentication,
    )
    serializer_class = LikeSerializer


class DeleteLikeApiView(generics.DestroyAPIView):
    queryset = Like.objects.all()
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    authentication_classes = (
        JSONWebTokenAuthentication,
        SessionAuthentication,
        BasicAuthentication,
    )
    serializer_class = LikeSerializer

    def get_queryset(self):
        return (
            Like.objects.filter(user=self.request.data.get("user"))
            .filter(post=self.request.data.get("post"))
            .first()
        )

    def destroy(self, request, *args, **kwargs):
        message = self.get_queryset()
        if not message:
            raise NotFound("This object is not exist")
        if not (request.user == message.user):
            raise NotFound("You have not permission")
        message.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class GetAnalyticsApiView(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    authentication_classes = (
        JSONWebTokenAuthentication,
        SessionAuthentication,
        BasicAuthentication,
    )
    filter_class = LikeFilter
    filter_backends = (filters.DjangoFilterBackend,)
    serializer_class = LikeAnalyticsSerializer

    def get_queryset(self):
        print(
            Like.objects.extra(select={"date": "date(api_like.liked_date)"})
            .values("date")
            .order_by("-liked_date")
            .annotate(likes=Count("pk"))
        )
        return (
            Like.objects.extra(select={"date": "date(api_like.liked_date)"})
            .values("date")
            .order_by("-liked_date")
            .annotate(likes=Count("pk"))
        )

    def get(self, request, *args, **kwargs):
        if not (self.request.data.get("user") == request.user.id):
            raise NotFound("You have not permission")
        return self.list(request, *args, **kwargs)


class GetUserActivity(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserActivitySerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        if request.user.id == kwargs.get("pk"):
            return self.retrieve(request, *args, **kwargs)
        else:
            raise NotFound("You have not permission")


def jwt_response_payload_handler(token, user=None, request=None):
    return {"token": token}


class MyObtainJSONWebToken(ObtainJSONWebToken):
    serializer_class = JSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.object.get("user") or request.user
            token = serializer.object.get("token")
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            user_query = User.objects.filter(username=user).first()
            if user_query:
                # update last_login when user get token
                user_query.last_login = now()
                user_query.save()
            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
