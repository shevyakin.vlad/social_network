from django.utils.timezone import now
from .models import User


def last_visit_middleware(get_response):
    def middleware(request):
        """
        Save the time of last request
        """
        response = get_response(request)

        if request.user.is_authenticated:
            user = User.objects.get(id=request.user.id)
            userLastVisit = User.objects.filter(id=user.id)
            if userLastVisit:
                userLastVisit.update(last_request=now())

        return response

    return middleware
