import traceback
from rest_framework import serializers
from rest_framework.exceptions import NotFound
from django.core.exceptions import ObjectDoesNotExist

from .models import User, Like, Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "password", "email", "first_name", "last_name")
        extra_kwargs = {"password": {"write_only": True}}
        read_only_fields = ("id",)

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ("title", "text", "author")

    def create(self, validated_data):
        if not (
            self.context.get("request").user.id == int(validated_data.get("author").id)
        ):
            raise NotFound("You have not permission")
        try:
            instance = Post.objects.create(**validated_data)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                "Got a `TypeError` when calling `%s.%s.create()`. "
                "This may be because you have a writable field on the "
                "serializer class that is not a valid argument to "
                "`%s.%s.create()`. You may need to make the field "
                "read-only, or override the %s.create() method to handle "
                "this correctly.\nOriginal exception was:\n %s"
                % (
                    Post.__name__,
                    Post._default_manager.name,
                    Post.__name__,
                    Post._default_manager.name,
                    self.__class__.__name__,
                    tb,
                )
            )
            raise TypeError(msg)

        return instance


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ("user", "post")

    def create(self, validated_data):
        id_from_data = validated_data.get("user").id
        if not (self.context.get("request").user.id == int(id_from_data)):
            raise NotFound("You have not permission")
        try:
            instance = Like.objects.get(**validated_data)
        except ObjectDoesNotExist:
            instance = Like.objects.create(**validated_data)
        return instance


class LikeAnalyticsSerializer(serializers.ModelSerializer):
    date = serializers.DateField()
    likes = serializers.IntegerField()

    class Meta:
        model = Like
        fields = ("date", "likes")


class UserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "last_login", "last_request")
