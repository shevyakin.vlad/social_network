from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from api.models import User, Post, Like


class PostTestCase(APITestCase):
    def setUp(self) -> None:
        for i in range(1, 3):
            user = User.objects.create(
                username=f"test{i}",
                first_name=f"Name{i}",
                last_name=f"Lname{i}",
            )
            user.set_password(f"testPassword")
            user.save()

    def _get_user_token(self, username="test", password="testPassword"):
        login_url = reverse("api:token_obtain_pair")
        response = self.client.post(
            login_url,
            {"username": username, "password": password},
            format="json",
        )
        response_data = response.json()

        return "JWT {}".format(response_data.get("token", ""))

    def test_create_post(self):
        create_post_url = reverse("api:create_post")
        user = User.objects.first()
        print(user.username)
        result = self.client.post(
            create_post_url,
            {"title": "testtitle", "text": "testtext", "author": user.pk},
            HTTP_AUTHORIZATION=self._get_user_token(username=user.username),
        )
        self.assertEqual(result.status_code, 201)
        # Negative tests with wrong author
        result = self.client.post(
            create_post_url,
            {"title": "testtitle", "text": "testtext", "author": user.pk + 1},
            HTTP_AUTHORIZATION=self._get_user_token(username=user.username),
        )
        self.assertEqual(result.status_code, 404)


class LikeTestCase(APITestCase):
    def setUp(self) -> None:
        for i in range(1, 3):
            user = User.objects.create(
                username=f"test{i}",
                first_name=f"Name{i}",
                last_name=f"Lname{i}",
            )
            user.set_password(f"testPassword")
            user.save()
            post = Post.objects.create(
                title=f"testtitle{i}", text=f"testtext{i}", author=user
            )
            post.save()

    def _get_user_token(self, username="test", password="testPassword"):
        login_url = reverse("api:token_obtain_pair")
        response = self.client.post(
            login_url,
            {"username": username, "password": password},
            format="json",
        )
        response_data = response.json()

        return "JWT {}".format(response_data.get("token", ""))

    def test_create_like(self):
        create_like_url = reverse("api:like")
        user = User.objects.first()
        print(user)
        result = self.client.post(
            create_like_url,
            {"user": user.pk, "post": 1},
            HTTP_AUTHORIZATION=self._get_user_token(username=user.username),
        )
        self.assertEqual(result.status_code, 201)
        # Negative
        result = self.client.post(
            create_like_url,
            {"user": user.pk + 1, "post": 1},
            HTTP_AUTHORIZATION=self._get_user_token(username=user.username),
        )
        self.assertEqual(result.status_code, 404)

    def test_delete_like(self):
        create_like_url = reverse("api:like")
        delete_like_url = reverse("api:unlike")
        user = User.objects.first()
        post = Post.objects.first()
        self.client.post(
            create_like_url,
            {"user": user.pk, "post": post.id},
            HTTP_AUTHORIZATION=self._get_user_token(username=user.username),
        )
        result = self.client.delete(
            delete_like_url,
            {"user": user.pk, "post": post.id},
            HTTP_AUTHORIZATION=self._get_user_token(username=user.username),
        )
        self.assertEqual(result.status_code, 204)
