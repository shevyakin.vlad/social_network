from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    last_login = models.DateTimeField(null=True)
    last_request = models.DateTimeField(null=True)
    REQUIRED_FIELDS: list = []


class Post(models.Model):
    title = models.CharField(max_length=250)
    text = models.TextField(max_length=2000)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Like(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="user_who_liked"
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="liked_post")
    liked_date = models.DateField(auto_now_add=True, null=True, blank=True)
