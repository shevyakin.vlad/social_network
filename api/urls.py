from django.urls import path
from rest_framework_jwt.views import refresh_jwt_token
from . import views

app_name = "api"

urlpatterns = [
    path("token/", views.MyObtainJSONWebToken.as_view(), name="token_obtain_pair"),
    path("token/refresh/", refresh_jwt_token, name="token_refresh"),
    path("signup/", views.CreateUserAPIView.as_view(), name="signup"),
    path("create_post/", views.CreatePostAPIView.as_view(), name="create_post"),
    path("like/", views.CreateLikeAPIView.as_view(), name="like"),
    path("unlike/", views.DeleteLikeApiView.as_view(), name="unlike"),
    path("analytics/", views.GetAnalyticsApiView.as_view(), name="analytics"),
    path("activity/<int:pk>", views.GetUserActivity.as_view(), name="activity"),
]
